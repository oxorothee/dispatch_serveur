#librairies
import socketio
from aiohttp import web

#projet
import data
import account

"""                  le dispatcheur

    ce module sert a gerer la connection d'un client a sont compte 
    puis repartir c'est requete sur le different serveur disponnible
    
    phase 1, test et esquisse du programme
"""



#variable global
serveur_name = []
serveur_sid = {}

#créé la database sql si elle n'existe pas
data.creat_database()

"""     admin
    
    la connection admin permet de gerer
                        - les etat des serveur
                        - creation de nouveau serveur
                        - bannissement d'un compte
"""


#créé le routeur
dispatcheur = socketio.AsyncServer()
app = web.Application()
dispatcheur.attach(app)
dispatcheur.async_handlers = True

async def index(request):
    with open('index.html') as f:
        print(f"index = {f.read()}")
        return web.Response(text=f.read(), content_type='text/html')


"""  +--------------------------------------------+  gestionnaire account  +--------------------------------------------+
    
    le gestionnaire account permet:
                - l'authentification a un compte
                - la création d'un compte
                - supprimer la connection de la liste des connction
    
    il verifie aussi:
            -que le client ne soit pas ban lors de l'authantification
    
"""

@dispatcheur.event
async def creataccount(sid,message):
    """
    création d'un compte
    :param sid:
    :param message:
    :return:
    """
    message = message.split("/")
    if len(message) != 5:
        await emit_log(sid,"creat_account|bad_requet")
    else:
        if await account.create_account(sid,message[0],message[1],message[2],message[3],message[4]):
            await emit_log(sid,"creat_account|succes")
            await auth(sid,message[0] + "|" + message[1])
        else:
            await emit_log(sid,"creat_account|echec")

@dispatcheur.event
async def auth(sid,message):
    """
    connection a un compte
    :param sid:
    :param message: str = pseudo|mdp
    :return:
    """
    message = message.split("|")
    if len(message) != 2: # le message n'est pas conforme a la procedure de connection
        await emit_log(sid,"auth|bad_request")

    if await account.account_connect(sid,pseudo=message[0],mdp=message[1]): #la connection au compte ce fait
        await emit_log(sid,"authent|succes")

    else: # la connection au coupte a echouer
        await emit_log(sid,"authent|echec")


@dispatcheur.event
async def disconnect(sid):
    """
    deconnection d'un client
    :param sid:
    :return:
    """
    global serveur_sid

    print(f'diconnect {sid}')
    if sid in account.dict_account_connected: # si c'est un client qui ce deconnect
        account.dict_account_connected.pop(account.dict_account_connected[sid][0])
        account.dict_account_connected.pop(sid)

    elif sid in serveur_sid: # si c'est un serveur qui ce deconnect
        serveur_sid[sid]["etat"] = "deconnecter"


@dispatcheur.event
async def connect(sid,env): #ce declenche quand une nouvelle connection ce fait, attention, a ce stade le client n'est pas connecter a un compte
    print(f'connect {sid}')


@dispatcheur.event
async def info_serveur(sid,message):
    pass


@dispatcheur.event
async def connect_to_serveur(sid,message):
    pass


"""  +--------------------------------------------+  event dispatcheur  +--------------------------------------------+


    les event dispatcheur sont des reception des client vers les serveur annex
    le dispatcheur verifie a chaque fois que:
                        - le client est bien connecter a sont compte
                        - le client a bien selectionner un serveur annex
                        - sur qu'elle serveur annex il doit renvoyer l'information
                        _ que le serveur annexe est disponnible
"""















"""  +--------------------------------------------+  renvoie de donner  +--------------------------------------------+
    
    le renvoie de donner permet de renvoyer des donner au clients
    celle si peuvent vennir du dispatcheur (ce script) ou des serveur annex
    
"""

async def emit_log(sid,message):

    """
    emet au client les log du dispatcheur

    :param sid: identifiant de l'utilisateur
    :param message: donner a communiquer
    """

    await dispatcheur.emit("log", message, to=sid)

@dispatcheur.event
async def log(sid, message, id):
    """
    emet les log depuis le serveur annex au client via sont id
    :param sid:
    :param message:
    :param id:
    :return:
    """
    if account.dict_account_connected[id][0]:
        print(f'log serveur ok')
        await dispatcheur.emit("log",message,to= account.dict_account_connected[id][0])
    else:
        print('log serveur erreur')


"""  +--------------------------------------------+  gestionnaire serveur  +--------------------------------------------+

    permet la connection au dispatcheur au serveur
    permet de connecter le clients au serveur
"""

@dispatcheur.event
async def serveurconnect(sid,message):
    """

    :param sid: sid du serveur
    :param message: [mot de passe,nom du serveur]
    """
    global serveur_sid
    global serveur_name

    if not data.control_serveur_exist(message[1]): # si le serveur n'est pas dans la database le serveur ne peut pas ce connecter
        await emit_log(sid,"univer_not_exist")
        return

    if not message[1] in serveur_name: # si le serveur exist deja la varible severname(permet au client de comuniquer avec le serveur
        serveur_name.append(message[1])
        serveur_sid[message[1]][0] = sid

    else: # si le serveur exist dans la database mais n'est pas dans la variable server_name
        serveur_info = data.info_serveur(message[1])
        serveur_sid[message[1]] = [sid,serveur_info[0],serveur_info[1],serveur_info[2],serveur_info[3]]
        serveur_name.append(message[1])








# active le routeur
app.router.add_get("/",index)
web.run_app(app)
