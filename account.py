# librairies
import sqlite3

#projet
from data import chemin_sqlite_database,id_generator


#variable global
dict_account_connected = {}



async def create_account(sip,pseudo,mdp,nom,prenom,email):
    """
    créé un compte utilisateur
    :return:
    """
    if not valide_info_creat_account(pseudo,mdp,nom,prenom,email):
        return False
    account = [str(pseudo),str(mdp),id_generator(),str(nom),str(prenom),str(email),False]
    try:
        connect = sqlite3.connect(chemin_sqlite_database)
        curseur = connect.cursor()

        curseur.execute("""
                        INSERT INTO account VALUES(
                        :pseudo,
                        :mdp,
                        :id,
                        :nom,
                        :prenom,
                        :email,
                        :ban
                        )
                        """,account)
        connect.commit()
        connect.close()
        return True
    except:
        print("erreur creation de compte")
        return False

def account_exist(pseudo):
    """
    control que le compte exist via le nom de compte, ne control pas que le mdp est le bon
    :return:
    """
    connect = sqlite3.connect(chemin_sqlite_database)
    cursor = connect.cursor()
    cursor.execute("SELECT * FROM account WHERE pseudo=?",(pseudo,))
    account = cursor.fetchall()
    connect.commit()
    connect.close()
    if account != None:
        return True
    return False

def account_mdp_good(pseudo,mdp):
    """
    control que le mdp est le bon pour autoriser la connection au compte
    :param pseudo:
    :param mdp:
    :return:
    """

    connect = sqlite3.connect(chemin_sqlite_database)
    cursor = connect.cursor()
    cursor.execute("SELECT * FROM account WHERE pseudo=?",(pseudo,))
    account = cursor.fetchone()
    connect.commit()
    connect.close()

    if mdp == account[1]:
        #la connection est autoriser
        return True
    print("erreur connection de compte")
    print(account)
    return False

async def account_connect(sid, pseudo, mdp):
    """
    control que le mdp est le bon, puis ajoute les donner
    :return:
    """
    global dict_account_connected
    if not account_exist(pseudo):
        return False

    if account_mdp_good(pseudo,mdp):
        id = id_by_pseudo(pseudo)
        dict_account_connected[sid] = [id,pseudo]
        dict_account_connected[id] = [sid, pseudo]
        return True
    return False



def valide_info_creat_account(pseudo,mdp,nom,prenom,email):
    """
    controlle que les info pour valider un compte soit exact,
    que le pseudo n'est pas deja pris, que l'email soit valide, que le mdp est correct(lettre majuscule,letre minuscul et un chiffre)
    :param pseudo:
    :param mdp:
    :param nom:
    :param prenom:
    :param email:
    :return: bool, true si les info sont valide, False dans le cas contraire
    """
    return True

def id_by_pseudo(pseudo):

    """
    retourn l'id d'un compte via sont pseudo
    :param pseudo:
    :return: str id
    """
    connect = sqlite3.connect(chemin_sqlite_database)
    cursor = connect.cursor()

    cursor.execute("SELECT * FROM account WHERE pseudo=?",(pseudo,))
    account = cursor.fetchone()

    connect.commit()
    connect.close()

    return account[2]

