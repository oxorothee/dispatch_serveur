#librairies
import json
import sqlite3
import random
import os

chemin_json_id = os.path.join(os.path.dirname(os.path.abspath(__file__)),"id_generate.json")
chemin_sqlite_database = os.path.join(os.path.dirname(os.path.abspath(__file__)),"database.db")


def id_generator():
    """
    créé un id unique pour chaque utilisateur
    :return: id
    """
    try:
        with open(chemin_json_id, "r") as f:
            data_id = json.load(f)
    except:
        data_id = []

    while(True):
        id = 'user' + str(random.randint(1000000000,9999999999))
        if id not in data_id:
            data_id.append(id)
            with open(chemin_json_id,'w') as f:
                json.dump(data_id,f)
            return id


def creat_database():
    """
    créé les table sqlite si elle n'existe pas
    """
    connect = sqlite3.connect(chemin_sqlite_database)
    curseur = connect.cursor()

    #table account
    curseur.execute("""
                    CREATE TABLE IF NOT EXISTS account(
                        pseudo STR,
                        mdp STR,
                        id STR,
                        prenom STR,
                        nom STR,
                        email STR,
                        ban BOOL
                    )""")

    #table univers
    curseur.execute("""
                    CREATE TABLE IF NOT EXISTS univers(
                        nom STR,
                        maintenance BOOL,
                        ouverture STR,
                        info STR
                    )""")

    # table ban
    curseur.execute("""
                    CREATE TABLE IF NOT EXISTS ban(
                        id STR,
                        definitif BOOL,
                        datefin STR
                    )""")

    # table id
    curseur.execute("""
                    CREATE TABLE IF NOT EXISTS id(
                        id STR
                    )""")

    connect.commit()
    connect.close()

def control_serveur_exist(name):
    """
    control que le serveur exist dans la database
    pour créé un serveur passer part le module admin qui n'est pas connecter au reste du script
    :param name: nom du serveur
    :return:
    """
    connect = sqlite3.connect(chemin_sqlite_database)
    cursor = connect.cursor()

    cursor.execute("SELECT * FROM univers WHERE nom=?",(name,))
    serveur = cursor.fetchone()
    connect.commit()
    connect.close()

    if serveur != None:
        return True
    return False

def info_serveur(name):
    """
    retourne le info du serveur depuis la database
    :param name: nom du serveur
    :return: list des atribut du serveur
    """

    connect = sqlite3.connect(chemin_sqlite_database)
    cursor = connect.cursor()

    cursor.execute("SELECT * FROM univers WHERE nom=?", (name,))
    serveur = cursor.fetchone()
    connect.commit()
    connect.close()
    return serveur